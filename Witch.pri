SOURCES += \
    src/witch.cpp \
    src/broom.cpp \
    src/wand.cpp \
    src/rune.cpp \
    src/grandwitch.cpp \
    src/witcher.cpp

HEADERS += \
    include/Witch \
    include/luckeymath.h \
    include/witch.h \
    include/rune.h \
    include/wand.h \
    include/broom.h \
    include/grandwitch.h \
    include/witcher.h
