#include "broom.h"

float Broom::angle(AngleUnit u) const
{
    float a{ static_cast<float>(QLineF::angle()) };

    a = offsetAngle(a, DEG);

    if (u != DEG)
        a = spokeTo(u, toSpoke(DEG, a));

    return a;
}

float Broom::mapIn(float a, AngleUnit u)
{
    return a - angle(u);
}

float Broom::mapOut(float a, AngleUnit u)
{
    a += spokeTo(u, (a < 0 ? 4 :
                            -4));

    return angle(u) - a;
}
