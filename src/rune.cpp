#include "rune.h"

Rung Rune::plot(float t) const
{
    float sinPiT{   sine(M_PI * t) };
    float cosPiT{ cosine(M_PI * t) };
    Rung ap{ lerped(t) };
    const float a{ ap.first  };
    const float p{ ap.second * 5.0f / M_PI };

    const float x{
        lerp(t, (1.0f - cosPiT) / 2.0f,
             a * a * (1.0f - p)) };

    const float y{
        lerp(0.0f, sinPiT / 2.0f,
             a) };

    return { x, y };
}

Rune Rune::spoke(float in, float out, float p1, float p2)
{
    using namespace std;

    in  = cycle( in, -4.0f, 4.0f);
    out = cycle(out, -4.0f, 4.0f);

    const float pinchIn { 1.0f + 0.5f * p1 };
    const float pinchOut{ 1.0f + 0.5f * p2 };
    float r1{ min(1.0f, max(-1.0f, pinchIn  * in  / 2)) };
    float r2{ min(1.0f, max(-1.0f, pinchOut * out / 2)) };

    // Beyond 2 spokes / 90 degrees / 100 gon / quarter pi radians

    if (abs(in)  > 2)
        p1 -= abs(in)  - abs(r1) * 2;
    if (abs(out) > 2)
        p2 -= abs(out) - abs(r2) * 2;

    if (abs(in)  > 3)
    {
        float s{ (r1 == 0.0f ? r1 :
                               r1 / abs(r1)) };

        r1 -= s * (abs(in) - abs(p1) * 3) / 3;
        p1 += (abs(in) - abs(r1) - 2) / 3;
    }
    if (abs(out) > 3)
    {
        float s{ (r2 == 0.0f ? r2 :
                               r2 / abs(r2)) };

        r2 -= s * (abs(out) - abs(p2) * 3) / 3;
        p2 += (abs(out) - abs(r2) - 2) / 3;
    }

    return { r1, r2,
             p1, p2 };
}
