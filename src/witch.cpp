#include "witch.h"

Witch::Witch(const Broom& broom, const Wand& wand): QPainterPath(),
    broom_{ broom },
    wand_{ wand }
{
    if (wand_.size())
        burn();
}


void Witch::burn(unsigned resolution)
{
    operator &=(QPainterPath{});

    if (wand_.empty() || broom_.length() == 0.0f)
        return;

    moveTo(broom_.p1());

    //Adaptive number of subdivisions
    if (resolution == 0)
        resolution = std::max<float>(1.0f, broom_.length() * 0.55f);


    Crook c{ wand_.normalizedCrook() };
    QPointF offset{};
    float angle{};

    for (int w{0}; w < wand_.size(); ++w)
    {
        Rune r{ wand_.at(w) };

        QMatrix branch{};
        float s{ c.get(Crook::LENGTH, w) };
        angle =  c.get(Crook::ANGLE,  w, DEG);

        branch.translate(offset.x(), offset.y());
        branch.scale(s, s);
        branch.rotate(angle);

        const float dt{ 1.0f / resolution };

        for (int i{ (r.isFlat() ? resolution : 1) };
             i <= resolution; ++i)
        {
            const Rung     p{ r.plot(i * dt) };
            const QPointF to{ p.first, p.second };

            lineTo((branch * matrix()).map(to));
        }

        offset += branch.map(QPointF{ 1.0f, 0.0f });
    }
}

void Witch::set(const Witch& witch)
{
    bool combust{ false };

    if (broom_ != witch.broom_)
    {
        if (broom_.length() != witch.broom_.length())
            combust |= true;

        broom_ = witch.broom_;
    }

    if (wand_ != witch.wand_)
    {
        wand_ = witch.wand_;
        combust |= true;
    }

    if (combust)
        burn();
}

QRegion Witch::bounds() const
{
    QPolygonF extremes{
        QRectF{ QPointF{ M_SQRT1_2 - 1, -0.5f },
                 QSizeF{ M_SQRT2      ,  1.0f } } };

    return QRegion{ matrix().map(extremes).boundingRect()
                   .adjusted(-UNIT, -UNIT, UNIT,  UNIT).toRect() };
}

float Witch::angleAt(const float t, AngleUnit u) const
{
    if (t != 0.0f && t != 1.0f)
    {
        return angle(u);// + wand_.angleAt(t, u);;
    }
    else
    {
        if (t == 0.0f)
            return angle(u) + wand_.in(u);
        else
            return angle(u) - wand_.out(u);
    }
}
