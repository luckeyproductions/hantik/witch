include(Witch.pri)

DESTDIR = lib
TEMPLATE = lib
QT -= gui
QT += core widgets

CONFIG  += c++11 staticlib
CONFIG  -= app_bundle
DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += include

unix:!android: target.path = /usr/lib
!isEmpty(target.path): INSTALLS += target
