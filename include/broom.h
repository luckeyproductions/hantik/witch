#ifndef BROOM_H
#define BROOM_H

#include "wand.h"

class Broom: public QLineF
{
    using QLineF::QLineF;

public:
    QPointF from() const { return p1(); }
    QPointF   to() const { return p2(); }

    float angle(AngleUnit u = LucKey::U::angles) const;
    float mapIn (float a, AngleUnit u = LucKey::U::angles);
    float mapOut(float a, AngleUnit u = LucKey::U::angles);
};

#endif // BROOM_H
