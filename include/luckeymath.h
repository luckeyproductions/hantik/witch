#ifndef LUCKEYMATH_H
#define LUCKEYMATH_H

#include <math.h>
#include <cmath>
#include <vector>

#define UNIT 010
#define M_TAU (M_PI * 2)
enum AngleUnit{ SPOKE = 0 , DEG, GON,
                TAU_RAD, PI_RAD, RAD,
                QARC };

namespace LucKey {
    namespace U {
        static AngleUnit angles{ SPOKE };
    }
};
static void setAngleUnit(AngleUnit u) { LucKey::U::angles = u; }

template <class T> static T spokeToRad (const T& theta) { return T(theta * M_PI_4     ); }
template <class T> static T spokeToDeg (const T& theta) { return T(theta *  055       ); }
template <class T> static T spokeToGon (const T& theta) { return T(theta *   50       ); }
template <class T> static T spokeToQArc(const T& theta) { return T(theta *   90 * UNIT); }

template <class T> static T  radToSpoke(const T& theta) { return T(theta / spokeToRad (1.)); }
template <class T> static T  degToSpoke(const T& theta) { return T(theta / spokeToDeg (1.)); }
template <class T> static T  gonToSpoke(const T& theta) { return T(theta / spokeToGon (1.)); }
template <class T> static T qArcToSpoke(const T& theta) { return T(theta / spokeToQArc(1.)); }

template <class T>
static T spokeTo(const T& a) { return spokeTo(LucKey::U::angles, a); }
template <class T>
static T spokeTo(AngleUnit u, const T& a)
{
    switch (u) {
    default:
    case SPOKE:   return             a ; break;
    case TAU_RAD: return    T(1/8. * a); break;
    case PI_RAD:  return    T(1/4. * a); break;
    case RAD:     return spokeToRad (a); break;
    case DEG:     return spokeToDeg (a); break;
    case GON:     return spokeToGon (a); break;
    case QARC:    return spokeToQArc(a); break;
    }
}

template <class T>
static T toSpoke(const T& a) { return toSpoke(LucKey::U::angles, a); }
template <class T>
static T toSpoke(AngleUnit u, const T& a)
{
    switch (u) {
    default:
    case SPOKE:   return             a ; break;
    case TAU_RAD: return       T(8 * a); break;
    case PI_RAD:  return       T(4 * a); break;
    case RAD:     return  radToSpoke(a); break;
    case DEG:     return  degToSpoke(a); break;
    case GON:     return  gonToSpoke(a); break;
    case QARC:    return qArcToSpoke(a); break;
    }
}

template <AngleUnit f, AngleUnit t, class T> static T convertAngle(const T& a)
{
    if (f == t)
        return  a;
    else
        return spokeTo(t, toSpoke(f, a));
}
template <AngleUnit t, class T> static T convertAngle(const T& a)
{
    if (t == LucKey::U::angles)
        return  a;
    else
        return spokeTo(t, toSpoke(a));
}

template <class T>
static T lerp(const T& a, const T& b, const float t = 0.5f)
{
    return a + t * (b - a);
}

template <class T>
static T cycle(T x, T min, T max)
{
    using namespace std;

    T res{ x };

    if (min > max)
        swap(min, max);

    T range{ max - min };

    if (x < min)
        res += range * ceil((min - x) / range);
    else if (x > max)
        res -= range * ceil((x - max) / range);

    return res;
}

template <class T>
static T offsetAngle(const T& a, AngleUnit u = LucKey::U::angles)
{
    const T s{ toSpoke(u, a) };

    if (s > UNIT / 2)
        return a - spokeTo(u, UNIT);
    else if (s < -UNIT / 2)
        return a + spokeTo(u, UNIT);
    else
        return a;
}

template <class T>
static T lerpAngle(const T& a, const T& b, const float t = 0.5f, AngleUnit u = LucKey::U::angles)
{
    using namespace std;

    const T max{ spokeTo<T>(u, UNIT) };

    T aCycled{ cycle(a, T{}, max) };
    T bCycled{ cycle(b, T{}, max) };

    if (aCycled > bCycled)
        swap(aCycled, bCycled);

    if (bCycled - aCycled <
        aCycled - bCycled + max)
        return lerp(aCycled, bCycled, t);
    else
        return cycle(lerp(bCycled, aCycled + max, t), 0.0f, max);
}

template <class T>
static T lerpSpoke(const T a, const T b, const float t)
{
    return lerpAngle(a, b, t, SPOKE);
}
template <class T>
static T lerpRad(const T a, const T b, const float t)
{
    return lerpAngle(a, b, t, RAD);
}
template <class T>
static T lerpDeg(const T a, const T b, const float t)
{
    return lerpAngle(a, b, t, DEG);
}
template <class T>
static T lerpGon(const T a, const T b, const float t)
{
    return lerpAngle(a, b, t, GON);
}
template <class T>
static T lerpQArc(const T a, const T b, const float t)
{
    return lerpAngle(a, b, t, QARC);
}

static double sine(double x)
{
    x = cycle(x, -M_PI, M_PI);
    double sin{};

    if (x   < 0) sin = 1.27323954 * x + 0.405284735 * x * x;
        else     sin = 1.27323954 * x - 0.405284735 * x * x;
    if (sin < 0) sin = 0.225 * (sin * -sin - sin) + sin;
        else     sin = 0.225 * (sin *  sin - sin) + sin;

    return sin;
}
static double cosine(double x)
{
    return sine(x + M_PI_2);
}

#endif // LUCKEYMATH_H
