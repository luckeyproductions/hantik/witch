#ifndef WITCHER_H
#define WITCHER_H

#include "grandwitch.h"

struct Witcher
{
    static Witch connect(const QPointF& from, const QPointF& to,
                         float in, float out, AngleUnit u = LucKey::U::angles)
    {
        Broom broom{ from, to };

        float inA { broom.mapIn (in,  u) };
        float outA{ broom.mapOut(out, u) };

        return Witch{ broom,
                    { Rune::spoke(toSpoke(u, inA),
                                  toSpoke(u, outA)) } };
    }

    static Witch connect(const Witch& witch, const QPointF& to,
                         float out, AngleUnit u = LucKey::U::angles)
    {
        return connect(witch.to(), to,
                       witch.outAngle(u), out,
                       u);
    }

    static Witch link(QWidget* a, QWidget* b)
    {
        return {};
    }

    static Witch lobe(const Witch& witch, const QPointF& through)
    {
        if (witch.wand().empty())
            return Witch{ witch.broom() };

        Broom bender{ witch.broom().from(), through };
        float kink{  offsetAngle(bender.angle() - witch.broom().angle()) };

        float lengthA{ witch.broom().length() == 0.0f ? 1.0f :
                       bender.length() / witch.broom().length()
                     };
        float lengthB{ 1.0f - lengthA };

        const Rune rune{ Rune::line() };//witch.wand().front() };
        const Rung crookA{ lengthA,  kink };
        const Rung crookB{ lengthB, -kink };
        Wand wand{ rune.split(rune.r0()) };
        wand.setCrook(0, crookA);
        wand.setCrook(1, crookB);

        return Witch{ witch.broom(), wand };
    }

    static Meet flame(const Witch& witch, float width)
    {
        return { witch.broom(), { witch.wand().front() },
                 lobe(witch, witch.center() + QPointF{ width, 0.0f } ).wand()
               };
    }
};

#endif // WITCHER_H
