#ifndef WITCH_H
#define WITCH_H

#include "broom.h"
#include "wand.h"

#include <QPainter>

class Witch: public QPainterPath
{
public:
    Witch(const Broom& broom, const Wand& wand);
    Witch(Broom broom = {{},{ 1.0f, 0.0f }}): Witch(broom, {}) {}

    virtual void burn(unsigned resolution = 0);

    void set(const Witch& witch);

    void setBroom(const Broom& broom) { broom_ = broom; }
    Broom  broom() const { return broom_; }
    QPointF from() const { return broom_.from(); }
    QPointF   to() const { return broom_.to(); }

    Wand wand() const           { return wand_;            }
    void clearWand()            { wand_.clear();           }
    void setRune(const Rune& r) { clearWand(); addRune(r); }
    void addRune(const Rune& r) { wand_.push_back(r);      }

    QMatrix matrix() const
    {
        QMatrix mat{};

        mat.translate(broom_.p1().x(),  broom_.p1().y());
        mat.scale(    broom_.length(), -broom_.length());
        mat.rotate(   broom_.angle(DEG));

        return mat;
    }

    QRect     rect() const { return bounds().boundingRect(); }
    QRegion bounds() const;

    float centerDistance(const QPointF& point) { return Broom{ point, center() }.length(); }
    QPointF center() const              { return broom_.pointAt(1/2.f);             }
    float radius() const                { return broom_.length() / 2;               }

    bool within(QPointF point) const
    {
        return Broom{ center(), point }.length() < radius();
    }

    float angleAt(const float t, AngleUnit u = LucKey::U::angles) const;
    float angle(   AngleUnit u = LucKey::U::angles) const { return broom_.angle(u); }
    float inAngle( AngleUnit u = LucKey::U::angles) const { return angleAt(0.0f, u); }
    float outAngle(AngleUnit u = LucKey::U::angles) const { return angleAt(1.0f, u); }

private:
    Broom broom_;
     Wand wand_;
};

using Mass = std::vector<Witch>;

class Pixie: public Witch
{
public:
    Pixie(const Broom& broom): Witch(broom),
      pix_{}
    {
    }
    Pixie(const Broom& broom, QPixmap* pix): Witch(broom),
      pix_{ *pix }
    {
    }
private:
    QPixmap pix_;
};

#endif // WITCH_H
