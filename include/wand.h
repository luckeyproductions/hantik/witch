#ifndef WAND_H
#define WAND_H

#include "rune.h"

#include <unordered_map>
#include <QLineF>

class Crook: public std::unordered_map<unsigned, Rung>
{
    using unordered_map::unordered_map;

public:
    Crook(): std::unordered_map<unsigned, Rung>()
    {
        operator[](0) = { 1.0f, 0.0f };
    }

    float length(unsigned n) const
    {
        float l{};

        for (unsigned i{0}; i < n; ++i)
        {
            l += get(LENGTH, i);
        }

        return l;
    }

    Crook normalized(unsigned n) const
    {
        if (n == 0)
            return Crook{};

        Crook c{};
        float span{ length(n) };

        for (unsigned i{0}; i < n; ++i)
        {
            c[i] = { get(LENGTH, i) / span,
                     get(ANGLE,  i) };
        }

        return c;
    }

    enum What{ LENGTH = 0, ANGLE };
    float get(What w, unsigned i, AngleUnit u = SPOKE) const
    {
        if (find(i) == end())
            return (w ? 0.0f : 1.0f);

        if (w == LENGTH)
            return at(i).first;
        else
            return spokeTo(u, at(i).second);
    }
};

class Wand: public std::vector<Rune>
{
    using vector::vector;

public:
    Wand(const Wand& w);
    Wand(const std::pair<Rune, Rune>& r): Wand({ r.first, r.second }) {}

    /// Should account for Crook
    float in (AngleUnit u = LucKey::U::angles) const
    {
        if (empty())
            return 0.0f;
        else
            front(). in(u);
    }
    float out(AngleUnit u = LucKey::U::angles) const
    {
        if (empty())
            return 0.0f;
        else
            back ().out(u);
    }

    void setCrook(unsigned c, Rung l, AngleUnit u = LucKey::U::angles)
    {
        crook_[c] = { l.first, toSpoke(u, l.second) };
    }
    void setCrook(const Wand& w)      { crook_ = w.crook(); }

    void uncrook() { crook_.clear(); }
    void uncrook(unsigned c)
    {
        if (hasCrook(c))
            crook_.erase(c);
    }

    bool hasCrook()           const { return !crook_.empty(); }
    bool hasCrook(unsigned c) const { return crook_.find(c) != crook_.end(); }

    Crook crook() const { return crook_; }
    Crook normalizedCrook() const
    {
        return crook_.normalized(size());
    }

    Rung crookAt(unsigned c) const
    {
        if (hasCrook(c))
            return crook_.at(c);
        else
            return Rung{ 1.0f, 0.0f };
    }


private:
    Crook crook_{};
};

#endif // WAND_H
