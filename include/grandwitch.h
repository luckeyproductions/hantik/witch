#ifndef GRANDWITCH_H
#define GRANDWITCH_H

#include "witch.h"

class Unimorph: public Witch // A multi-broom witch
{
public:
    void burn(unsigned resolution = 0) override;

private:
    std::vector<Broom> polybroom_;
};

class Foil: public Unimorph // Polygonal multi-broom witch
{
};


class GrandWitch: public Mass, public Witch // A collection of witches, treated as one
{
    using vector::vector;
public:
    void burn(unsigned resolution = 0) override = 0;
};


class Meet: public GrandWitch // Two opposing witches
{
    enum Side{ PORT = 0, STAR };

public:
    Meet(): GrandWitch{ Witch{},
                        Witch{} }
    {}

    Meet(const Broom& b, const Wand& p, const Wand& s):
        GrandWitch({ Witch{ b, p },
                     Witch{ b, s } })
    {}

    void burn(unsigned resolution = 0) override;

    void setInWidth(float w)  { width_.first  = w; }
    void setOutWidth(float w) { width_.second = w; }
    void updateBrooms() {}

    float  inWidth() const { return width_.first;  }
    float outWidth() const { return width_.second; }

private:
    Witch& port() { return at(PORT); }
    Witch& star() { return at(STAR); }

    Rung width_{};
};

class Callis: public GrandWitch // Meet polyline
{
public:
    void burn(unsigned resolution = 0) override;
};

class Coven: public GrandWitch // Witch polygon
{
    void burn(unsigned resolution = 0) override;

    QPolygon roundTable_;
};

#endif // GRANDWITCH_H
