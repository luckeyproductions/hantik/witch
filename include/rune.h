#ifndef RUNE_H
#define RUNE_H

#include "luckeymath.h"

using Rung = std::pair<float, float>;
class Rune: public std::vector<float>
{
    enum Glyph{ Rin = 0, Rout, Pin, Pout };
    float&       at(Glyph g)              { return operator[](g); }
    const float& at(Glyph g) const        { return operator[](g); }
    float&       at(bool o, bool p = 0)       { return at(static_cast<Glyph>(o + p * 2)); }
    const float& at(bool o, bool p = 0) const { return at(static_cast<Glyph>(o + p * 2)); }

public:
    Rune(): std::vector<float>(4u)
    {}

    Rune(float r1, float r2,
         float p1 = 0.0f, float p2 = 0.0f):
        std::vector<float>({ r1, r2, p1, p2 })
    {}

    static Rune       line() { return Rune{ 0, 0 }; }
    static Rune semicircle() { return Rune{ 1, 1 }; }
    static Rune     corner() { return Rune{ 1, 1, 1, 1}; }
    static Rune   parabola() { return Rune{ 1/2., 1/2., 0.19, 0.19 }; }
    static Rune     beauty() { return Rune::gon( -87.5, 75 ); }

    Rung plot(float t) const;

    float rin () { return at(Rin ); }
    float rout() { return at(Rout); }
    float pin () { return at(Pin ); }
    float pout() { return at(Pout); }
    float rin () const { return at(Rin ); }
    float rout() const { return at(Rout); }
    float pin () const { return at(Pin ); }
    float pout() const { return at(Pout); }

    Rung lerped(const float t) const
    {
        return { lerp(rin(), rout(), t),
                 lerp(pin(), pout(), t) };
    }

    float inSpoke() const
    {
        float pinch{ 1.0f - 0.5f * pin() };
        return rin() * pinch * 2.f;
    }

    float outSpoke() const
    {
        float pinch{ 1.0f - 0.5f * pout() };
        return rout() * pinch * 2.f;
    }

    float in (AngleUnit u = LucKey::U::angles) const { return spokeTo(u,  inSpoke()); }
    float out(AngleUnit u = LucKey::U::angles) const { return spokeTo(u, outSpoke()); }

    float  inDeg () const { return in (DEG); }
    float outDeg () const { return out(DEG); }
    float  inRad () const { return in (RAD); }
    float outRad () const { return out(RAD); }
    float  inGon () const { return in (GON); }
    float outGon () const { return out(GON); }
    float  inQArc() const { return in (QARC); }
    float outQArc() const { return out(QARC); }

    static Rune spoke(float in, float out, float p1 = 0.0f, float p2 = 0.0f);
    static Rune deg(float in, float out, float p1 = 0.0f, float p2 = 0.0f)
    {
        return Rune::spoke(toSpoke(DEG, in ),
                           toSpoke(DEG, out),
                           p1, p2);
    }
    static Rune rad(float in, float out, float p1 = 0.0f, float p2 = 0.0f)
    {
        return Rune::spoke(toSpoke(RAD, in ),
                           toSpoke(RAD, out),
                           p1, p2);
    }
    static Rune gon(float in, float out, float p1 = 0.0f, float p2 = 0.0f)
    {
        return Rune::spoke(toSpoke(GON, in ),
                           toSpoke(GON, out),
                           p1, p2);
    }
    static Rune qArc(float in, float out, float p1 = 0.0f, float p2 = 0.0f)
    {
        return Rune::spoke(toSpoke(QARC, in ),
                           toSpoke(QARC, out),
                           p1, p2);
    }

    static Rune vary(Rune ma = corner(), unsigned steps = 010)
    {
        return  vary(ma.inverted(), ma, steps);
    }
    static Rune vary(Rune mi, Rune ma, unsigned steps = 010)
    {
        if (!steps)
            return ma;

        Rune r{};

        for (int y: {0,1})
        for (int x: {0,1})
        {
            const float s{ (1.0f / steps) * (random() % (steps + 1)) };

            r.at(x, y) = lerp(mi.at(x,y), ma.at(x,y), s);
        }

        return r;
    }

    Rune inverted() const
    {
        return { -rin(), -rout(),
                  pin(),  pout() };
    }

    Rune reversed() const
    {
        return { rout(), rin(),
                 pout(), pin() };
    }

    float r0() const /// Needs some thought
    {
        return 0.5f - abs(rin()) - abs(rout()) / 2;
    }
    std::pair<Rune, Rune> split(float t = 0.5f) const /// Needs some thought
    {
         Rung rung{ lerped(t) };
        float rMid{     (rung.first + (in() - out()) ) / 10 };
        float pMid{ lerp(rung.second, -abs(rMid) / 4) };

        return {
                 Rune{ rin(), rMid,
                       pin(), pMid },

                 Rune{ -rMid, rout(),
                        pMid, pout() }
               };
    }

    bool isFlat() { return rin() == 0 && rout() == 0; }
};

#endif // RUNE_H
