#include "tryall.h"

TryAll::TryAll(QWidget *parent): QMainWindow(parent),
    court_{ new Court{ this } }
{
    setWindowTitle("Salem");
    setWindowIcon(QIcon{ ":/Curve" });
    setCentralWidget(court_);
}

TryAll::~TryAll()
{
}
