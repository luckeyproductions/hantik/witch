#ifndef COURT_H
#define COURT_H

#include <QDebug>
#include <QDateTime>
#include <Witch>

#include <QWidget>

enum Side{ Left = 0, Top, Right, Bottom, Center };
enum WitchType{ Curve = 0, Lobe, Flame };

class Member: public QPointF
{
    using QPointF::QPointF;
    friend class Court;

    QPoint position(const QSize& s) {
        return QPointF{ s.width () * x(),
                        s.height() * y()
                      }.toPoint();
    }

    QRect rect(const QSize& s)
    {
        return QRect{ position(s) - QPoint{ UNIT<<1, UNIT<<1 },
                      QSize{ 1, 1 } * (UNIT<<2)
                    };
    }

    void turn()
    {
        turn(90.0f * side_);
    }

    void turn(float deg)
    {
        if (side_ == Top || side_ == Right)
            deg = -deg;

        angle_ += deg;

        if (side_ != Center)
        {
            Rung n{ neck() };
            bool flip{ n.first > n.second };

            if (!flip)
            {
                if (angle_ < n.first)
                    angle_ = n.first;

                else if (angle_ > n.second)
                         angle_ = n.second;
            }
            else
            {
                if (angle_ > 0.0f
                 && angle_ < n.first)
                    angle_ = n.first;

                else if (angle_ < 0.0f
                      && angle_ > n.second)
                         angle_ = n.second;
            }
        }

        angle_ = offsetAngle(angle_);
    }

    Rung neck() const
    {
        switch (side_) {
        default:
        case Left:   return { -100,  100 }; break;
        case Top:    return {  200,    0 }; break;
        case Right:  return {  100, -100 }; break;
        case Bottom: return {    0,  200 }; break;
        }
    }

    float angle_{ (random() % 200) - 100.0f };
    Side side_{ Center };
};

using Jury = std::vector<Member*>;

class Court: public QWidget
{
    Q_OBJECT

    QPoint position(Member* m) {
    return m->position(size());
    }
    QPoint center() {
    return { width () / 2,
             height() / 2 };
    }

public:
    explicit Court(QWidget* parent = nullptr);

protected:
    void paintEvent(QPaintEvent*      e) override;
    void resizeEvent(QResizeEvent*    e) override;
    void mouseMoveEvent(QMouseEvent*  e) override;
    void mousePressEvent(QMouseEvent* e) override;
    void mouseReleaseEvent(QMouseEvent* e) override;
    void wheelEvent(QWheelEvent* e) override;

private:
    void drawJury();
    void refreshWitch()
    {
        switch (bench_) {
        default:
        case Curve:
            witch_->set(Witcher::connect(position(from_), position(to_),
                                         from_->angle_, to_->angle_));
        break;
        case Lobe:
            witch_->set(Witcher::lobe(Witcher::connect(position(from_), position(to_),
                                                       from_->angle_, to_->angle_),
                                      position(&bigwig_)));
        break;
        case Flame:
            witch_->set(Witcher::flame(*witch_,
                                       (witch_->center().toPoint() - position(&bigwig_)).x()));
        break;
        }
    }

     QImage room_;
       Jury jury_;
    Member* active_;
    Member* from_;
    Member* to_;
     Witch* witch_;

     Member bigwig_{ 0.5f, 0.5f };
     WitchType bench_{ Curve };

     bool animate_;
#define TIMESTEP 0.023f
public slots:
     void converse()
     {
         float t{ TIMESTEP };
         float d{ 0.0001f * (QDateTime::currentMSecsSinceEpoch() % 10000) };

         for (Member* m: jury_)
         {
             if (m != &bigwig_)
             {
                 int n{ std::distance(jury_.begin(),
                                      std::find(jury_.begin(), jury_.end(), m)) };
                 m->turn(50 * sine(M_TAU * d * (1 + (n % 3)) + n) * t);
             }
             else
             {
                 m->turn(t * 23.0f);
             }

         }

         refreshWitch();
         repaint();
     }
};

#endif // COURT_H
